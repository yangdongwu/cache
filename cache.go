package cache

import (
	"gitee.com/aesoper/cache/factory"
	"gitee.com/aesoper/cache/memcache"
	"gitee.com/aesoper/cache/redis"
	"gitee.com/aesoper/cache/memory"
	"gitee.com/aesoper/cache/ssdb"
)

type Cfg struct {
	// 启用的缓存驱动 值为 redis|redis_cluster|memcache
	Driver       string               `mapstructure:"driver" json:"driver"`
	Redis        redis.Options        `mapstructure:"redis" json:"redis"`
	RedisCluster redis.ClusterOptions `mapstructure:"redisCluster" json:"redisCluster"`
	Memcache     memcache.Options     `mapstructure:"memcache" json:"memcache"`
	SSDB         ssdb.Options         `mapstructure:"ssdb" json:"ssdb"`
	Memory       memory.Option        `mapstructure:"memory" json:"memory"`
}

func New(cfg Cfg) (c factory.Cache, err error) {
	var op factory.CacheOption
	if len(cfg.Driver) == 0 {
		cfg.Driver = "memory"
	}
	switch cfg.Driver {
	case "redis":
		op = redis.WithRedisOptions(cfg.Redis)
		break
	case "redis_cluster":
		op = redis.WithRedisClusterOptions(cfg.RedisCluster)
		break
	case "memcache":
		op = memcache.WithMemcacheOption(cfg.Memcache)
		break
	case "ssdb":
		op = ssdb.WithSSDBOptions(cfg.SSDB)
		break
	case "memory":
		op = memory.WithMemoryOption(cfg.Memory)
		break
	default:
		op = redis.WithRedisOptions(cfg.Redis)
	}

	return factory.NewCache(cfg.Driver, op)
}
